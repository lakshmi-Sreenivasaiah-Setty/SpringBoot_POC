package com.example.truistdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.example.truistdemo.*"})
public class TruistdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(TruistdemoApplication.class, args);
	}

}
