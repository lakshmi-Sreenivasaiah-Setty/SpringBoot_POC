package com.example.truistdemo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Value("${example.username}")
    String userName;

    @GetMapping("/api")
    public String hello_api(){
        return "Hello"+" "+userName;
    }
}
