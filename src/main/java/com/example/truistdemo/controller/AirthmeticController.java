package com.example.truistdemo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AirthmeticController {

    private final static Logger logger = LoggerFactory.getLogger(AirthmeticController.class);

    @GetMapping("/add")
    public Integer addIntegers(@RequestParam Integer number1,@RequestParam Integer number2){
        Integer sum = number1 + number2;
        logger.info("Sum of 2 Numbers:"+ sum);
        return  sum;
    }

    @GetMapping("/diff")
    public Integer subIntegers(@RequestParam Integer number1,@RequestParam Integer number2){
        Integer diff = 0;
        if(number1 >= number2)
            diff = number1 - number2;
         else
            diff = number2 - number1;
        logger.info("Difference of 2 Numbers:"+ diff);
        return  diff;
    }

    @GetMapping("/multiply")
    public Integer multipyIntegers(@RequestParam Integer number1,@RequestParam Integer number2){
        Integer mul = number1 * number2;
        logger.info("Multiplication of 2 Numbers:"+ mul);
        return  mul;
    }

    @GetMapping("/div")
    public Integer divIntegers(@RequestParam Integer number1,@RequestParam Integer number2){
        Integer div = 0;
        if(number1 >= number2)
            div = number2/number1;
        else
            div = number1/number2;
        logger.info("Division of 2 Numbers:"+ div);
        return  div;
    }
}
